
input_select:
  washing_machine_status:
    name: Washing Machine Status
    options:
      - Idle
      - Running
      - Finishing
      - Clean
    initial: Idle
  dryer_status:
    name: Dryer Status
    options:
      - Idle
      - Running
      - Finishing
      - Clean
    initial: Idle

# sensor:
# # TP-Link power readings
#   - platform: template
#     sensors:
#       plug1_amps:
#         friendly_name_template: "{{ state_attr('switch.plug1', 'friendly_name') }} Current"
#         value_template: "{{ state_attr('switch.plug1', 'current_a') | float }}"
#         unit_of_measurement: 'A'
#       plug1_watts:
#         friendly_name_template: "{{ state_attr('switch.plug1', 'friendly_name') }} Current Consumption"
#         value_template: "{{ state_attr('switch.plug1', 'current_power_w') | float }}"
#         unit_of_measurement: 'W'
#       plug1_total_kwh:
#         friendly_name_template: "{{ state_attr('switch.plug1', 'friendly_name') }} Total Consumption"
#         value_template: "{{ state_attr('switch.plug1', 'total_energy_kwh') | float }}"
#         unit_of_measurement: 'kWh'
#       plug1_volts:
#         friendly_name_template: "{{ state_attr('switch.plug1', 'friendly_name') }} Voltage"
#         value_template: "{{ state_attr('switch.plug1', 'voltage') | float }}"
#         unit_of_measurement: 'V'
#       plug1_today_kwh:
#         friendly_name_template: "{{ state_attr('switch.plug1', 'friendly_name') }} Today's Consumption"
#         value_template: "{{ state_attr('switch.plug1', 'today_energy_kwh') | float }}"
#         unit_of_measurement: 'kWh'

automation:
# When power is detected, and the washing machine is not in
# the Running state, change the status of the washing machine
# to Running.
# The status check will ensure we don't try to put the state
# to Running each time the power level changes, and we're already
# in the Running state.


############
## Washer ##
############

  - alias: Laundry - Set washing machine active when power detected
    trigger:
      - platform: numeric_state
        entity_id: sensor.plug1_current_consumption
        above: 10
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.washing_machine_status
          option: Running

# When the power drops, move the state of the washing machine to
# Finishing.

  - alias: Laundry - Set washing machine finished when power drops
    trigger:
      - platform: numeric_state
        entity_id: sensor.plug1_current_consumption
        below: 6
    condition:
      - condition: state
        entity_id: input_select.washing_machine_status
        state: Running
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.washing_machine_status
          option: Finishing

# Wait 1 minutes for us to be in the Finishing state before we
# decide the washing machine has finished. This way, if the
# washing machine is in between cycles and the power drops, we
# won't mark the washing machine cycle as finished too early.

  - alias: Laundry - Set washing machine clean after timeout
    trigger:
      - platform: state
        entity_id: input_select.washing_machine_status
        to: Finishing
        for:
          minutes: 1
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.washing_machine_status
          option: Clean

# When the washing machine door is opened, reset the status back to
# idle, so we don't spam people that the washing machine has
# finished, and someone has already emptied it

  - alias: Laundry - Set washing machine Idle when door opens
    trigger:
      - platform: state
        entity_id: binary_sensor.washing_machine_door_sensor
        to: 'on'
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.washing_machine_status
          option: Idle

###########
## Dryer ##
###########

  - alias: Laundry - Set dryer active when power detected
    trigger:
      - platform: numeric_state
        entity_id: sensor.aeon_labs_zw095_home_energy_meter_gen5_power
        above: 100
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.dryer_status
          option: Running

  - alias: Laundry - Set dryer finished when power drops
    trigger:
      - platform: numeric_state
        entity_id: sensor.aeon_labs_zw095_home_energy_meter_gen5_power
        below: 50
    condition:
      - condition: state
        entity_id: input_select.dryer_status
        state: Running
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.dryer_status
          option: Finishing

  - alias: Laundry - Set dryer clean after timeout
    trigger:
      - platform: state
        entity_id: input_select.dryer_status
        to: Finishing
        for:
          minutes: 1
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.dryer_status
          option: Clean

  - alias: Laundry - Set dryer Idle when door opens
    trigger:
      - platform: state
        entity_id: binary_sensor.dryer_door_sensor
        to: 'on'
    action:
      - service: input_select.select_option
        data:
          entity_id: input_select.dryer_status
          option: Idle

##############
## Combined ##
##############

# Laundry Alerts
  - alias: Laundry - Send alert when laundry is finished
    trigger:
      - platform: state
        entity_id: input_select.washing_machine_status
        to: Clean
        for:
          minutes: 1
      - platform: state
        entity_id: input_select.dryer_status
        to: Clean
        for:
          minutes: 1
      - platform: state
        entity_id: person.chris
        to: 'home'
        for:
          minutes: 5
      - platform: state
        entity_id: person.jen
        to: 'home'
        for:
          minutes: 5
    condition:
      - condition: time
        after: '08:30:00'
        before: '21:30:00'
      - condition: or
        conditions:
          - condition: and
            conditions:
              - condition: state
                entity_id: input_select.washing_machine_status
                state: Clean
              - condition: state
                entity_id: input_select.dryer_status
                state:
                  - Idle
                  - Clean
          - condition: and
            conditions:
              - condition: state
                entity_id: input_select.dryer_status
                state: Clean
              - condition: state
                entity_id: input_select.washing_machine_status
                state:
                  - Idle
                  - Clean
      - condition: template
        value_template: >
          {% if state_attr('automation.laundry_send_alert_when_laundry_is_finished', 'last_triggered') != None -%}
            {% if as_timestamp(now()) | int - as_timestamp(state_attr('automation.laundry_send_alert_when_laundry_is_finished', 'last_triggered')) | int > 1800 %}true{% else %}false{% endif %}
          {%- else -%}
            true
          {%- endif %} 
    action:
      - service: tts.cloud_say
        data:
          entity_id: media_player.broadcast_group
          message: 'Hey, the laundry has finished'

  - alias: Laundry - Water Leak Alarm
    trigger:
      - platform: state
        entity_id: 
          - binary_sensor.leak_sensor_water_leak_detected
          - binary_sensor.leak_sensor_water_leak_detected_2
        to: "on"
    action:
      - service: notify.notify
        data:
          title: Water Leak Detected
          message: Water detected at the washing machine
